const FLOOR_SEGMENT_WIDTH = 512;
const FLOOR_SEGMENT_HEIGHT = 512;
const COLORS = [
    0x21042D, 0x6F0C74, 0x25006D, 0x0E00CA, 0xEE1F97,
    0x86008A, 0x4B0034, 0x2703EC, 0x00BDFF, 0xB8C9EE
];

const KEY_LEFT = 37;
const KEY_RIGHT = 39;
const KEY_UP = 38;
const KEY_DOWN = 40;

var previousTime = 0;
var keys = {};
var prevKeys = {};
var scene = null;
var renderer = null;
var camera = null;
var player = null;
var collisions = [];
var level = {
    nextEntityId: 0,

    shipEntities: [],
    collisionEntities: [],
    meshEntities: [],
    trailEntities: [],
};

function isPressed(keyCode) {
    return keys[keyCode] > 0;
}

function isJustPressed(keyCode) {
    return isPressed(keyCode) && prevKeys[keyCode] == 0;
}

function radians(deg) {
    return deg * Math.PI/180;
}

function addEntity(entity) {
    if (entity.mesh) {
        level.meshEntities.push(entity);
        scene.add(entity.mesh);
    }
    if (entity.collision) {
        level.collisionEntities.push(entity);
    }
}

function createEntity(comps) {
    var position;
    if (comps.mesh) {
        position = comps.mesh.position;
    } else {
        position = new THREE.Vector3();
    }

    return Object.assign({
        id: level.nextEntityId++,
        kinematic: false,
        position: position,
        velocity: new THREE.Vector3()
    }, comps);
}

function createFloor() {
    var texture = new THREE.TextureLoader().load("checker.png");
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.anisotropy = renderer.getMaxAnisotropy();
    texture.repeat.set(FLOOR_SEGMENT_WIDTH/64, FLOOR_SEGMENT_HEIGHT/64);

    var geometry = new THREE.PlaneGeometry(FLOOR_SEGMENT_WIDTH, FLOOR_SEGMENT_HEIGHT);
    var material = new THREE.MeshBasicMaterial({map: texture, color: COLORS[5]});
    var mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);
}

function createShip() {
    function createShipFace(color, a, b, c) {
        var face = new THREE.Face3(a, b, c);
        for (var i = 0; i < 3; i++) {
            var c = color;
            if (i == 0) {
                c = COLORS[2];
            }
            face.vertexColors[i] = new THREE.Color(c);
        }
        return face;
    }

    const h = 0.5;
    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(-1, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, 0.1, h));
    geometry.vertices.push(new THREE.Vector3(0, 1, 0.1));

    geometry.vertices.push(new THREE.Vector3(1, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, 1, 0.1));
    geometry.vertices.push(new THREE.Vector3(0, 0.1, h));

    geometry.vertices.push(new THREE.Vector3(-1, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, 0.1, 0));
    geometry.vertices.push(new THREE.Vector3(0, 0.1, h));

    geometry.vertices.push(new THREE.Vector3(1, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, 0.1, h));
    geometry.vertices.push(new THREE.Vector3(0, 0.1, 0));

    var color = COLORS[3];
    geometry.faces.push(createShipFace(color, 0, 1, 2));
    geometry.faces.push(createShipFace(color, 3, 4, 5));
    geometry.faces.push(createShipFace(color, 6, 7, 8));
    geometry.faces.push(createShipFace(color, 9, 10, 11));
    geometry.computeFaceNormals();
    geometry.computeBoundingBox();

    var material = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.y = 3;
    mesh.position.z = 0.25;

    player = createEntity({
        mesh: mesh,
        collision: createCollisionBounds(),
    });
    addEntity(player);
}

function init() {
    scene = new THREE.Scene();
    scene.add(new THREE.AmbientLight(0xffffff));

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000);
    camera.position.z = 3;
    camera.up.set( 0, 0, 1 );
    camera.lookAt(new THREE.Vector3(0,20,0));

    createFloor();
    createShip();
}

function handleCollision(c) {
    return false;
}

function update(delta) {
    var dir = 0;
    if (isPressed(KEY_LEFT)) {
        dir = -1;
    } else if (isPressed(KEY_RIGHT)) {
        dir = 1;
    }

    if (isPressed(KEY_UP)) {
        player.mesh.position.y += 5 * delta;
    }
    player.mesh.position.x += 5 * dir * delta;

    level.meshEntities.forEach(updateEntityBounds);
    detectCollisions(collisions, level.collisionEntities);
    collisions.forEach(function(c) {
        if (handleCollision(c)) {
            resolveCollision(c);
        }
    });
    integrate(0, level.collisionEntities, delta);

    renderer.render(scene, camera);
}

function mainLoop() {
    time = new Date().getTime();

    update((time - previousTime) / 1000);

    for (var k in keys) {
        prevKeys[k] = keys[k];
    }
    previousTime = time;
    window.requestAnimationFrame(mainLoop);
}

document.addEventListener('keydown', function(e) {
    keys[e.keyCode] += 1;
});

document.addEventListener('keyup', function(e) {
    keys[e.keyCode] = 0;
});

document.addEventListener('DOMContentLoaded', function onContentLoaded() {
    var time = 0;

    init();
    window.requestAnimationFrame(mainLoop);
});
