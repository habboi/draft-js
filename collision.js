var dif = new THREE.Vector3();
function isColliding(apos, a, bpos, b) {
    dif.copy(bpos);
    dif.sub(apos);

    var dx = a.half.x + b.half.x - Math.abs(dif.x);
    if (dx <= 0) {
        return null;
    }

    var dy = a.half.y + b.half.y - Math.abs(dif.y);
    if (dy <= 0) {
        return null;
    }

    var dz = a.half.z + b.half.z - Math.abs(dif.z);
    if (dz <= 0) {
        return null;
    }

    var c = {};
    if (dx < dy && dx < dz && dy > 0.1) {
        c.normal.x = Math.sign(dif.x);
        c.depth = dx;
    } else if (dy < dz || dy < 0.1) {
        c.normal.y = Math.sign(dif.y);
        c.depth = dy;
    } else {
        c.normal.z = Math.sign(dif.z);
        c.depth = dz;
    }
    return c;
}

function createCollisionBounds() {
    return {
        center: null,
        half: null
    };
}

function updateEntityBounds(entity) {
    if (!entity.collision) {
        return;
    }
    var mesh = entity.mesh;
    var size = new THREE.Vector3();
    size.copy(mesh.geometry.boundingBox.max);
    size.sub(mesh.geometry.boundingBox.min);

    var half = new THREE.Vector3();
    half.x = size.x * 0.5;
    half.y = size.y * 0.5;
    entity.collision.center = entity.position;
    entity.collision.half = half;
}

function detectCollisions(collisions, entities) {
    var entityCount = entities.length;
    for (var i = 0; i < entityCount; i++) {
        var entityA = entities[i];
        for (var j = i+1; j < entityCount; j++) {
            var entityB = entities[i];
            var c = isColliding(entityA.position, entityA.collision, entityB.position, entityB.collision);
            if (c) {
                collisions.push(c);
            }
        }
    }
}

function applyVelocity(entity, vel) {
    if (!entity.kinematic) {
        entity.velocity.add(vel);
    }
}

function applyCorrection(entity, pos) {
    if (!entity.kinematic) {
        entity.position.add(pos);
    }
}

var rv = new THREE.Vector3();
function resolveCollision(c) {
    rv.copy(c.second.velocity);
    rv.sub(c.first.velocity);
    var velNormal = rv.dot(c.normal);
    if (velNormal > 0.0) {
        return;
    }

    var j = -1 * velNormal;
    rv.copy(c.normal);
    rv.multiplyScalar(j);
    applyVelocity(c.second, rv);
    applyVelocity(c.first, rv.negate());

    var s = Math.max(col.depth - 0.01, 0);
    rv.copy(c.normal);
    rv.multiplyScalar(s);
    applyCorrection(c.second, rv);
    applyCorrection(c.first, rv.negate());
}

var velTimesDelta = new THREE.Vector3();
function integrate(gravity, entities, delta) {
    entities.forEach(function(e) {
        if (e.kinematic) {
            return;
        }

        e.velocity.z += gravity * delta;
        velTimesDelta.copy(e.velocity);
        velTimesDelta.multiplyScalar(delta);
        e.position.add(velTimesDelta);
    });
}
